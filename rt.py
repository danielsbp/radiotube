import tkinter as tk
import pytube #Baixa as músicas do YouTube

#Para tocar as músicas
from pydub import AudioSegment
from pydub.playback import play as pdplay

import os
import random
import time

import threading

#Variável responsável por mostrar qual música está tocando
window = tk.Tk()
musica = tk.StringVar()

# PLAYLISTS:

#Melodic Dubstep -> https://www.youtube.com/playlist?list=PL47GfNryB12ujCf8ydXt71pnoiHnsN9iq
#House -> https://www.youtube.com/playlist?list=PL_Q15fKxrBb5d4FzxegXGGkW2eAgtukpi
#Future Bass -> https://www.youtube.com/playlist?list=PLe8jmEHFkvsbRwwi0ode5c9iMQ2dyJU3N
#Future Bounce -> https://www.youtube.com/playlist?list=PLKLu6ADZ8dQwixBiOHSSQv7m9S--_NxbF

 
def play(url):
	global musica
	
	playlist = pytube.Playlist(url)

	#Transforma a playlist em lista e depois
	#Reordena de forma aleatória os links
	#Fazendo assim a função de músicas aleatórias

	playlist = list(playlist)
	random.shuffle(playlist)

	video_file_name = 'video'

	for video in playlist:
	
		youtube_video = pytube.YouTube(video)

		musica.set('Baixando '+youtube_video.title)
		youtube_video.streams.filter(only_audio=True).first().download(filename=video_file_name) #COLOCA O FILENAME

		print(youtube_video.title)
		musica.set('Você está ouvindo '+youtube_video.title)

		sound = AudioSegment.from_file("video.mp4", format="mp4")
		pdplay(sound)

		os.system('rm video.mp4')
	
	musica.set('Sua playlist acabou!')
	render_widgets()


def pp():
	global musica
	url = ent_lp.get()
	if ("https://www.youtube.com/playlist?list=" in url):
		lbl_dlp.pack_forget()
		btn_play.pack_forget()
		ent_lp.pack_forget()

		lbl_mus.pack()

		trhead1 = threading.Thread(target=play,args=(url,))
		trhead1.start()

	else:
		lbl_erro = tk.Label(window, text="Erro: Link inválido. Coloque um link do YouTube!")
		lbl_erro.pack()


window.geometry('600x210')
window.title('RadioTube')
lbl_logo = tk.Label(window, text="RadioTube", fg="#8c190a", font="helvetica 18 bold", height=3)
lbl_dlp = tk.Label(window, text="Cole aqui o link de sua playlist do YouTube \n para ouví-la como um rádio!")

musica.set('Carregando...')

lbl_mus = tk.Label(window, textvariable=musica, fg="#8c190a", font="helvetica 14")
ent_lp = tk.Entry(window, width="230")
btn_play = tk.Button(window, text="Começar", command=pp, bg="#8c190a", fg="#fff", font="helvetica 18 bold", height=1)

def render_widgets():
	lbl_logo.pack(side="top")
	lbl_dlp.pack()
	ent_lp.pack()
	btn_play.pack()

render_widgets()

window.mainloop()
