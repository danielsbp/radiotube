# RadioTube (Programa em desenvolvimento) 
**Transforme sua playlist do YouTube numa rádio.**

### Como o RadioTube funciona?
Basicamente, você insere numa caixa de texto o seu link de playlist do YouTube. Então o programa distribui os vídeos de maneira aleatória, baixa e executa uma música de cada vez. (Obs: Depois que a música é tocada, o software )

### Qual o objetivo do RadioTube?
Meu computador é fraco, então eu sempre sofro ao abrir o navegador para colocar uma playlist do YouTube enquanto estou fazendo alguma outra coisa, porque os navegadores costuma consumir uma boa parte do computador enquanto é executado em um PC fraco. Pensando nisso, criei este software para "economizar" um pouco mais de processamento e memória RAM, já que o RadioTube é muito mais leve que um navegador.

### Requisitos para rodar o RadioTube:
- **Sistema Operacional Linux** (Por enquanto)
- **Linguagem de Programação**
    - [Python 3.8.5](https://www.python.org/)

- **Bibliotecas:**
    - [TKinter](https://docs.python.org/3/library/tkinter.html) 
    - [Pytube](https://pypi.org/project/pytube/)
    - [Pydub](https://pypi.org/project/pydub/)

### Instalando as Bibliotecas
Você pode executar o seguinte comando para instalar todas as bibliotecas de uma vez: `pip install -r requirements.txt`

### Rodando o RadioTube
Execute `python rt.py` no seu terminal para abrir o software 
